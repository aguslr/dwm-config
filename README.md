# dwm configuration

## About

This repository stores my configuration of [dwm][], *a dynamic window manager
for X that manages windows in tiled, monocle and floating layouts*.

## Getting the files

### Using Git

If we have Git installed on the system, we can get the files directly from the
repository:

    git clone https://gitlab.com/aguslr/dwm-config

After this, every time we want to update the files we do:

    cd dwm-config && git pull

### Without Git

If Git is not installed, we can still get the files as long as we have a basic
Unix environment available:

    wget https://gitlab.com/aguslr/dwm-config/-/archive/main/dwm-config-main.tar.gz -O - | tar -xzv --strip-components 1 --exclude={README.md,screenshot.png}

## Installing with Stow

To easily create and manage links to the files we can use GNU [Stow][] as
follows:

    stow -vt ~ -S .

## Screenshot

![Screenshot](https://gitlab.com/aguslr/dwm-config/raw/main/screenshot.png "Screenshot")

[dwm]: https://dwm.suckless.org/
[stow]: https://www.gnu.org/software/stow/
